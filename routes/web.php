<?php

use App\Http\Controllers\ArchivoController;
use App\Http\Controllers\InvitadoController;
use App\Http\Controllers\ReunionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::middleware(['auth'])->group(function () {
    Route::get('/home', [ReunionController::class, 'index'])->name('home');
    Route::get('/reunion/{id}', [ReunionController::class, 'show'])->name('reunion.show');
    Route::get('/create-reunion', [ReunionController::class, 'create'])->name('reunion.create');
    Route::post('/create-reunion', [ReunionController::class, 'store'])->name('reunion.store');
    Route::get('/edit-reunion/{id}', [ReunionController::class, 'edit'])->name('reunion.edit');
    Route::patch('/edit-reunion/{id}', [ReunionController::class, 'update'])->name('reunion.update');
    Route::delete('/delete-reunion/{id}', [ReunionController::class, 'destroy'])->name('reunion.delete');

    Route::get('/add-invitados/{id}', [InvitadoController::class, 'create'])->name('invitado.create');
    Route::post('/add-invitados', [InvitadoController::class, 'store'])->name('invitado.store');
    Route::get('/edit-invitados/{id}', [InvitadoController::class, 'edit'])->name('invitado.edit');
    Route::patch('/edit-invitados/{id}', [InvitadoController::class, 'update'])->name('invitado.update');

    Route::get('/add-files/{id}', [ArchivoController::class, 'create'])->name('archivo.create');
    Route::post('/add-files', [ArchivoController::class, 'store'])->name('archivo.store');
    Route::get('/edit-files/{id}', [ArchivoController::class, 'edit'])->name('archivo.edit');
    Route::patch('/edit-files/{id}', [ArchivoController::class, 'update'])->name('archivo.update');
});
