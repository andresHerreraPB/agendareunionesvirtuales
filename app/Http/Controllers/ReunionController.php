<?php

namespace App\Http\Controllers;

use App\Models\Reunion;
use Illuminate\Http\Request;

class ReunionController extends Controller
{
    public function index(){
        $reuniones = Reunion::all();
        return view('panel.reunion.index', compact('reuniones'));
    }
    public function create(){
        return view('panel.reunion.create');
    }
    public function show($id){
        $reunion = Reunion::find($id);
        return view('panel.reunion.show', compact('reunion'));
    }
    public function store(Request $request){
        $request->validate([
            'titulo_reunion'=>'required',
            'fecha_inicio'=>'required',
            'fecha_termino'=>'required',
            'descripcion'=>'required',
            'url_reunion'=>'required',
        ]);

        $reunion = new Reunion();
        $reunion->titulo = $request['titulo_reunion'];
        $reunion->fecha_inicio = $request['fecha_inicio'];
        $reunion->fecha_termino = $request['fecha_termino'];
        $reunion->descripcion = $request['descripcion'];
        $reunion->url_reunion = $request['url_reunion'];
        $reunion->save();

        return redirect()->route('home')->with('creado','Reunion creada!');
    }

    public function edit($id){
        $reunion = Reunion::find($id);
        return view('panel.reunion.edit', compact('reunion'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'titulo_reunion'=>'required',
            'fecha_inicio'=>'required',
            'fecha_termino'=>'required',
            'descripcion'=>'required',
            'url_reunion'=>'required',
        ]);

        Reunion::where('id_reunion', $id)->update([
            'titulo' => $request['titulo_reunion'],
            'fecha_inicio' => $request['fecha_inicio'],
            'fecha_termino' => $request['fecha_termino'],
            'descripcion' => $request['descripcion'],
            'url_reunion' => $request['url_reunion']
        ]);

        return redirect()->route('home')->with('editado','Reunion editada!');
    }

    public function destroy($id){
        $reunion = Reunion::find($id);
        $reunion->delete();

        return redirect()->route('home')->with('eliminado','Reunion eliminada!');
    }
}
