<?php

namespace App\Http\Controllers;

use App\Models\Invitado;
use App\Models\Reunion;
use Illuminate\Http\Request;

class InvitadoController extends Controller
{
    public function create($id){
        $reunion = Reunion::find($id);
        return view('panel.invitados.create', compact('reunion'));
    }

    public function store(Request $request){
        $request->validate([
            'nombres_invitados' => 'required'
        ]);

        $noInvitados = $request['nombres_invitados'];
        for ($i=0; $i < count($noInvitados); $i++) { 
            $invitado = new Invitado();
            $invitado->nombre_invitado = $noInvitados[$i];
            $invitado->id_reunion = $request['id_reunion'];
            $invitado->save();
        }

        return redirect()->route('reunion.show', [$request['id_reunion']]);
    }

    public function edit($id){
        $invitados = Reunion::find($id);
        return view('panel.invitados.edit', compact('invitados'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'nombres_invitados' => 'required'
        ]);
        
        Invitado::where('id_reunion',$id)->delete();

        $noInvitados = $request['nombres_invitados'];
        for ($i=0; $i < count($noInvitados); $i++) { 
            $invitado = new Invitado();
            $invitado->nombre_invitado = $noInvitados[$i];
            $invitado->id_reunion = $id;
            $invitado->save();
        }

        return redirect()->route('reunion.show', [$id]);
    }
}
