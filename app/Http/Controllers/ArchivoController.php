<?php

namespace App\Http\Controllers;

use App\Models\Archivo;
use App\Models\Reunion;
use Illuminate\Http\Request;

class ArchivoController extends Controller
{
    public function create($id){
        $reunion = Reunion::find($id);
        return view('panel.archivos.create', compact('reunion'));
    }

    public function store(Request $request){
        $request->validate([
            'archivos' => 'required'
        ]);

        //Si se desea almacenar archivos, se debe generar una carpeta 
        //dentro del storage para guardarse, apuntando hacia el path correspondiente
        $noArchivos = $request['archivos'];
        for ($i=0; $i < count($noArchivos); $i++) { 
            $invitado = new Archivo();
            $invitado->nombre_archivo = $noArchivos[$i]->getClientOriginalName();
            $invitado->id_reunion = $request['id_reunion'];
            $invitado->save();
        }

        return redirect()->route('reunion.show', [$request['id_reunion']]);
    }

    public function edit($id){
        $archivos = Reunion::find($id);
        return view('panel.archivos.edit', compact('archivos'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'archivos' => 'required'
        ]);
        
        Archivo::where('id_reunion',$id)->delete();

        $noArchivos = $request['archivos'];
        for ($i=0; $i < count($noArchivos); $i++) { 
            $invitado = new Archivo();
            $invitado->nombre_archivo = $noArchivos[$i]->getClientOriginalName();
            $invitado->id_reunion = $id;
            $invitado->save();
        }

        return redirect()->route('reunion.show', [$id]);
    }
}
