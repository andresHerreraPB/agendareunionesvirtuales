<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $primaryKey = 'id_reunion';
    protected $table = 'reuniones';

    public function archivos(){
        return $this->hasMany(Archivo::class, 'id_reunion');
    }

    public function invitados(){
        return $this->hasMany(Invitado::class, 'id_reunion');
    }

}
