@extends('templates.app')

@section('content')
<div class="container">
    <div class="card my-4">
        <div class="card-header bg-secondary text-white">Crear nueva reunion</div>

        <div class="card-body py-3">
            <form action="{{ route('reunion.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="form-row">
                    <div class="col py-2">
                        <label for="titulo_reunion">Titulo de la reunion</label>
                        <input type="text" class="form-control @error('titulo_reunion') is-invalid @enderror"
                            id="titulo_reunion" name="titulo_reunion" value="{{old('titulo_reunion')}}">
                        @error('titulo_reunion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col py-2">
                        <label for="fecha_inicio">Fecha y hora de inicio</label>
                        <input type="datetime-local" class="form-control @error('fecha_inicio') is-invalid @enderror"
                            id="fecha_inicio" name="fecha_inicio" value="{{old('fecha_inicio')}}">
                        @error('fecha_inicio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col py-2">
                        <label for="fecha_termino">Fecha y hora de termino</label>
                        <input type="datetime-local" class="form-control @error('fecha_termino') is-invalid @enderror"
                            id="fecha_termino" name="fecha_termino" value="{{old('fecha_termino')}}">
                        @error('fecha_termino')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col py-2">
                        <label for="descripcion">Descripcion detallada</label>
                        <textarea class="form-control @error('descripcion') is-invalid @enderror" id="descripcion"
                            rows="3" name="descripcion">{{old('descripcion')}}</textarea>
                        @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col py-2">
                        <label for="url_reunion">URL de la reunion</label>
                        <input type="text" class="form-control @error('url_reunion') is-invalid @enderror"
                            id="url_reunion" name="url_reunion" value="{{old('url_reunion')}}">
                        @error('url_reunion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <div class="btn-group my-3" role="group">
                            <button class="btn btn-primary" type="submit">Crear reunion</button>
                            <a href="{{ route('home') }}" class="btn btn-danger">Regresar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection