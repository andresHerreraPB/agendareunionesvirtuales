@extends('templates.app')

@section('content')
<div class="container">
    <div class="card my-5">
        <div class="card-header bg-secondary text-white">Listado de reuniones virtuales</div>

        <div class="card-body py-3">
            <a href="{{ route('reunion.create') }}" class="btn btn-secondary mt-2 mb-4">Crear nueva reunion</a>
            <div class="table-responsive">
                @if(session('creado'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Creado</strong> {{session('creado')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif(session('editado'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Editado</strong> {{session('editado')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif(session('eliminado'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Eliminado</strong> {{session('eliminado')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr align="center">
                            <th>Fecha</th>
                            <th>Titulo</th>
                            <th>URL de la reunion</th>
                            <th>Invitados</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reuniones as $reunion)
                        <tr>
                            <td><small>{{$reunion->fecha_inicio}} - {{$reunion->fecha_termino}}</small></td>
                            <td><small>{{$reunion->titulo}}</small></td>
                            <td><small>{{$reunion->url_reunion}}</small></td>
                            <td>
                                @if (count($reunion->invitados) > 0)
                                <small>
                                    @foreach ($reunion->invitados as $invitado)
                                    {{$invitado->nombre_invitado}} -
                                    @endforeach
                                </small>
                                @else
                                <small>
                                    Ninguno
                                </small>
                                @endif
                            </td>
                            <td align="center">
                                <a href="{{ route('reunion.show', $reunion->id_reunion) }}" class="btn btn-sm">Ver</a>
                                <a href="{{ route('reunion.edit', $reunion->id_reunion) }}"
                                    class="btn btn-sm">Editar</a>
                                <form action="{{ route('reunion.delete', $reunion->id_reunion) }}" class="delete-form"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm">Cancelar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready( function () {
     $('.table').DataTable();
 } );

 $(".delete-form").submit(function(e) {
    e.preventDefault();
    Swal.fire({
            title: 'Desea cancelar esta reunion?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
        }).then(resultado => {
            if(resultado.value){
                this.submit();
            }
        });
});
</script>
@endsection