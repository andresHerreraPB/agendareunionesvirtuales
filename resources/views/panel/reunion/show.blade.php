@extends('templates.app')

@section('content')
<div class="container">
    <div class="card my-5">
        <div class="card-header bg-secondary text-white">Datos de la reunion No.{{$reunion->id_reunion}}</div>

        <div class="card-body py-3">
            <label for="">Titulo de la reunion</label>
            <p><strong>{{$reunion->titulo}}</strong></p>
            <hr>
            <label for="">Fecha y hora de inicio</label>
            <p><strong>{{$reunion->fecha_inicio}}</strong></p>
            <hr>
            <label for="">Fecha y hora de termino</label>
            <p><strong>{{$reunion->fecha_termino}}</strong></p>
            <hr>
            <label for="">Descripcion detallada</label>
            <p><strong>{{$reunion->descripcion}}</strong></p>
            <hr>
            <label for="">URL de la reunion</label>
            <p><a href="{{$reunion->url_reunion}}"><strong>{{$reunion->url_reunion}}</strong></a></p>
            <hr>
            <label for="">Invitados</label>
            <ul>
                @if (count($reunion->invitados) > 0)       
                @foreach ($reunion->invitados as $invitado)
                <li>{{$invitado->nombre_invitado}}</li>
                @endforeach
                <a href="{{ route('invitado.edit', $reunion->id_reunion) }}" class="btn btn-sm btn-warning float-right">
                    editar
                </a>
                @else
                <p>No hay invitados creados</p>
                <a href="{{ route('invitado.create', $reunion->id_reunion) }}" class="btn btn-sm btn-success float-right">
                    agregar
                </a>
                @endif
            </ul>
            <hr>
            <label for="">Archivos</label>
            
            <ul>
                
                @if (count($reunion->archivos) > 0)
                @foreach ($reunion->archivos as $archivo)
                <li><a href="#">{{$archivo->nombre_archivo}}</a></li>
                @endforeach
                <a href="{{ route('archivo.edit', $reunion->id_reunion) }}" class="btn btn-sm btn-warning float-right">
                    editar
                </a>
                @else
                <p>No hay archivos asignados</p>
                <a href="{{ route('archivo.create', $reunion->id_reunion) }}" class="btn btn-sm btn-success float-right">
                    agregar
                </a>
                @endif
            </ul>

            <hr>
            <a href="{{ route('home') }}" class="btn btn-danger">Regresar</a>
        </div>
    </div>
</div>
@endsection