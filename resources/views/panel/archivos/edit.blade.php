@extends('templates.app')

@section('content')
<div class="container">
    <div class="card my-4">
        <div class="card-header bg-secondary text-white">Editar invitados</div>

        <div class="card-body py-3">
            <form action="{{ route('archivo.update', $archivos->id_reunion) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="row">
                    <div class="col-lg-12">
                        <div id="newRow">
                            @foreach ($archivos->archivos as $archivo)
                            <div id="inputFormRow">
                                <div class="input-group mb-3">
                                    <input type="file" name="archivos[]" class="form-control m-input" value="{{$archivo->nombre_archivo}}" required>
                                    <div class="input-group-append">
                                        <button id="removeRow" type="button" class="btn btn-danger">&times;</button>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <button id="addRow" type="button" class="btn btn-info">Agregar</button>
                    </div>
                    @error('archivos')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="col-lg-12 text-center">
                        <div class="btn-group my-3" role="group">
                            <button class="btn btn-primary" type="submit">Guardar cambios</button>
                            <a href="{{ route('home') }}" class="btn btn-danger">Regresar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
// agregar registro
$("#addRow").click(function () {
$('#newRow').append('<div id="inputFormRow">'+
'<div class="input-group mb-3">'+
'<input type="file" name="archivos[]" class="form-control m-input" required>'+
'<div class="input-group-append">'+
'<button id="removeRow" type="button" class="btn btn-danger">&times;</button>'+
'</div>'+
'</div>');
});

// borrar registro
$(document).on('click', '#removeRow', function () {
$(this).closest('#inputFormRow').remove();
});
});
</script>
@endsection