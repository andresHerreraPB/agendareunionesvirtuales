
<h1>PROCESO PARA EJECUTAR PROYECTO LARAVEL:</h1>

<h3>-----Pasos:</h3>
<ol>
<li>Descargar repositorio en la carpeta de ejecucion de proyectos de su servidor local</li>
<li>Mediante la terminal/CMD ejecutar el comando 'composer install' en la ruta del proyecto, esto para descargar la dependencias del mismo</li>
<li>dentro del proyecto, copiar y pegar el archivo '.env.example' y agregarle al nuevo archivo el nombre de '.env'</li>
<li>Mediante la terminal/CMD ejecutar el comando 'php artisan key:generate' en la ruta del proyecto</li>
<li>Mediante la terminal/CMD ejecutar el comando 'php artisan migrate --seed' en la ruta del proyecto para cargar la BD <small>nombre de la BD 'agendareunionesvirtuales'</small></li>
</ol>


<h3>-----NOTA:</h3>
<strong>Los usuarios se generan a traves de los factories de laravel, es decir que los correos para iniciar sesion varian, se tiene que consultar en la tabla users algun correo registrado e ingresar con la contraseña 'password'</strong>


